from VVPD4 import find_wrong_letters
import pytest


def test_1_letter():
    assert find_wrong_letters('AASDFG') == 'A'


def test_2_letters():
    assert find_wrong_letters('AASSDFG') == 'AS'


def test_3_letter():
    assert find_wrong_letters('AASSDDFG') == 'ASD'


def test_0_letter():
    assert find_wrong_letters('ASDFG') == 'Broken characters not found'
