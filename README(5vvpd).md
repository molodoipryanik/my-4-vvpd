# 5 vvpd

Сначала нужно было выполнить поставленные задачи:

1. Разделить программу на три фунции:
   * функция проверки ввода.
   * функция выполнения прораммы.
   * функция вывода результата программы на экран.
2. Написать документацию к функциям и тестам.
3. Создать с помощью Doxygen html страничку с документацией.
4. Оформить readme.md, в котором можно будет увидеть этапы проделанной работы.

Смысл программы заключается в том, чтобы найти во входной строке сломанные буквы.
Сломанными считаются дублирующиеся буквы.

_Функция_ __*вывода*__
```
main():
    print(find_wrong_letters(check_input(input())))
```

_Функция_ __*проверки ввода*__
```
def check_input(word):
    list_of_letters_check = list(word)
    if len(list_of_letters_check) == 0:
        raise ValueError()
    for check_word in list_of_letters_check:
        if ord(check_word) < 65 or ord(check_word) > 90:
            raise ValueError()
    return word
```

_Функция_ __*выполнения задания*__
```
def find_wrong_letters(word):
    broken = ''
    list_of_letters = list(word)
    if len(list_of_letters) > 1:
        for index in range(len(set(list_of_letters))):
            letter_buff, list_of_letters[index] = (list_of_letters[index],
                                                   'letter')
            while list_of_letters.count(letter_buff) > 0:
                list_of_letters.remove(letter_buff)
            list_of_letters[index] = letter_buff
    for letter in list_of_letters:
        if word.count(letter) % 2 == 0:
            count_of_occurrences = word.count(letter * 2)
            if count_of_occurrences == word.count(letter) / 2:
                broken += letter
    if broken == '':
        broken = 'Broken characters not found'
    return broken
```
[А ты любишь readme-файлы, _Джотаро_?](https://www.youtube.com/watch?v=mC3mhnmr9bE)

<img src="http://pm1.narvii.com/7263/a2c65e225c805180083c4f60a758443116cf64c5r1-1920-1080v2_uhq.jpg">
