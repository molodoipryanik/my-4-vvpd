from VVPD4 import check_input
import pytest


def test_correct_input():
    assert check_input('AASSDDFF') == 'AASSDDFF'


def test_wrong_letters_input():
    with pytest.raises(ValueError):
        check_input('dsadwsdaASDW')


def test_another_symbols_input():
    with pytest.raises(ValueError):
        check_input('%$@(%@!')


def test_numbers_input():
    with pytest.raises(ValueError):
        check_input('13224312')


def test_empty_input():
    assert check_input('') == ''
