var tests_8py =
[
    [ "test_0_letters", "tests_8py.html#a1725c9a2434882f91b11c677640de3d4", null ],
    [ "test_1_letter", "tests_8py.html#a9e6492488e277bacdc0ee7d2ec3fc553", null ],
    [ "test_2_2_letters", "tests_8py.html#a01c1d62113db4df6212824edb29f663c", null ],
    [ "test_2_3_letters", "tests_8py.html#a7e226b777c7646b8b55362c6696d57ce", null ],
    [ "test_2_4_letters", "tests_8py.html#abc5bb8e02641a71125b639458c8f54d6", null ],
    [ "test_2_letters", "tests_8py.html#a4274fc61a1c165cd070d63366117c759", null ],
    [ "test_3_letters", "tests_8py.html#aa14394e4e36254944211514694a3a682", null ],
    [ "test_another_symbols_input", "tests_8py.html#a54a07c639fffb70798a02567d58b1735", null ],
    [ "test_correct_input", "tests_8py.html#ab01130290d3a26d840dfaa9e59f231fe", null ],
    [ "test_empty_input", "tests_8py.html#ae907ef3647b6bbab2dd7ecdf76719cb4", null ],
    [ "test_numbers_input", "tests_8py.html#ae1b1dc24271d79e00fa6f78674f65115", null ],
    [ "test_wrong_letters_input", "tests_8py.html#ac12a46bbb953ab11c5de2ed6d3c7d7fe", null ]
];