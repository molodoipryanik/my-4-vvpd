def check_input(word):
    list_of_letters_check = list(word)
    for check_word in list_of_letters_check:
        if ord(check_word) < 65 or ord(check_word) > 90:
            raise ValueError()
    return word


def find_wrong_letters(word):
    broken = ''
    list_of_letters = list(word)
    if len(list_of_letters) > 1:
        for index in range(len(set(list_of_letters))):
            letter_buff, list_of_letters[index] = (list_of_letters[index],
                                                   'letter')
            while list_of_letters.count(letter_buff) > 0:
                list_of_letters.remove(letter_buff)
            list_of_letters[index] = letter_buff
    for letter in list_of_letters:
        if word.count(letter) % 2 == 0:
            count_of_occurrences = word.count(letter * 2)
            if count_of_occurrences == word.count(letter) / 2:
                broken += letter
    if broken == '':
        broken = 'Broken characters not found'
    return broken


def main():
    print(find_wrong_letters(check_input(input())))


if __name__ == '__main__':
    main()
